package main
 import(
	"time"
	"log"
	"flag"
	"context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	pb "example.com/m/event_listener_spec"
	models "example.com/m/models"

	// models "example.com/m/models"
 )

func main(){

	type Data struct {
		PeerID        string `json:"peer_id"`
		CPU           int    `json:"cpu"`
		RAM           int    `json:"ram"`
		Network       int    `json:"network"`
		DedicatedTime int    `json:"dedicated_time"`
		Timestamp     int    `json:"timestamp"`
	}
	var Data_info Data
	Data_info.PeerID = "test-peer-id-123456778"
	Data_info.CPU = 4342
	Data_info.RAM = 1231
	Data_info.Network = 76
	Data_info.DedicatedTime = 12
	Data_info.Timestamp = 4543452
	var (
		addr = flag.String("addr", "localhost:50051", "the address to connect to")
	)

	xlr := Data{ PeerID:"new_peer_id"}
	log.Println(xlr.PeerID)
	conn, err := grpc.Dial(*addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	client := pb.NewEventListenerClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	res, err := client.NewDeviceOnboarded(ctx, &pb.NewDeviceOnboardedInput{ PeerId:"sdsfdsff"})

	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	log.Printf("Greeting: %s", res.PeerId)
 }
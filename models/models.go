package main

	
type NewDeviceOnboarded struct {
	PeerID        string `json:"peer_id"`
	CPU           int    `json:"cpu"`
	RAM           int    `json:"ram"`
	Network       int    `json:"network"`
	DedicatedTime int    `json:"dedicated_time"`
	Timestamp     int    `json:"timestamp"`
}

	
type DeviceStatusChange struct {
	PeerID    string  `json:"peer_id"`
	Status    string  `json:"status"`
	Timestamp float64 `json:"timestamp"`
}

type DeviceResourceChange struct {
	PeerID                   string `json:"peer_id"`
	ChangedAttributeAndValue struct {
		CPU           float64 `json:"cpu"`
		RAM           float64 `json:"ram"`
		Network       float64 `json:"network"`
		DedicatedTime float64 `json:"dedicated_time"`
	} `json:"changed_attribute_and_value"`
	Timestamp float64 `json:"timestamp"`
}

type DeviceResourceConfig struct {
	PeerID                   string `json:"peer_id"`
	ChangedAttributeAndValue struct {
		CPU           float64 `json:"cpu"`
		RAM           float64 `json:"ram"`
		Network       float64 `json:"network"`
		DedicatedTime float64 `json:"dedicated_time"`
	} `json:"changed_attribute_and_value"`
	Timestamp float64 `json:"timestamp"`
}

type NewService struct {
	ServiceID          string  `json:"service_id"`
	ServiceName        string  `json:"service_name"`
	ServiceDescription string  `json:"service_description"`
	Timestamp          float64 `json:"timestamp"`
}

type ServiceStatus struct {
	ServiceID           string  `json:"service_id"`
	PeerID              string  `json:"peer_id"`
	OnlineOfflineStatus string  `json:"online_offline_status"`
	Timestamp           float64 `json:"timestamp"`
}

type ServiceCall struct {
	CallID              int64  `json:"call_id"`
	PeerIDOfServiceHost string `json:"peer_id_of_service_host"`
	ServiceID           string `json:"service_id"`
	CPUUsed             int    `json:"cpu_used"`
	MemoryUsed          int    `json:"memory_used"`
	NetworkBwUsed       int    `json:"network_bw_used"`
	TimeTaken           int    `json:"time_taken"`
	Status              string `json:"status"`
	Timestamp           int64  `json:"timestamp"`
}

type ServiceRun struct {
	CallID              string `json:"call_id"`
	PeerIDOfServiceHost string `json:"peer_id_of_service_host"`
	Status              string `json:"status"`
	Timestamp           int    `json:"timestamp"`
}

type ServiceRemove struct {
	ServiceID string  `json:"service_id"`
	Timestamp float64 `json:"timestamp"`
}

	
type NtxPayment struct {
	ServiceID   string  `json:"service_id"`
	AmountOfNtx float64 `json:"amount_of_ntx"`
	PeerID      string  `json:"peer_id"`
	Timestamp   float64 `json:"timestamp"`
}